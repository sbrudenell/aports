# Maintainer: Ariadne Conill <ariadne@dereferenced.org>
pkgname=pkgconf
pkgver=2.0.2
pkgrel=0
pkgdesc="development framework configuration tools"
url="https://gitea.treehouse.systems/ariadne/pkgconf"
arch="all"
license="ISC"
replaces="pkgconfig"
provides="pkgconfig=1"
checkdepends="kyua atf"
subpackages="$pkgname-doc $pkgname-dev"
source="https://distfiles.ariadne.space/pkgconf/pkgconf-$pkgver.tar.xz
	"

# secfixes:
#   1.9.4-r0:
#     - CVE-2023-24056

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-pkg-config-dir=/usr/local/lib/pkgconfig:/usr/local/share/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	ln -s pkgconf "$pkgdir"/usr/bin/pkg-config
}

dev() {
	default_dev

	# Move pkg-config back to main package (default_dev implicitly moves
	# files /usr/bin/*-config to -dev).
	mv "$subpkgdir"/usr/bin/pkg-config "$pkgdir"/usr/bin/

	mkdir -p "$pkgdir"/usr/share/aclocal/
	mv "$subpkgdir"/usr/share/aclocal/pkg.m4 "$pkgdir"/usr/share/aclocal/
}

sha512sums="
ca0570cff61534508b091408edf0021773c5f7f4c57ec5427474242f5f84a37e8fdc220cc02b9b362e71b6f8735f0be2c2c246e2212c65a833e44182e2e12e32  pkgconf-2.0.2.tar.xz
"
