# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=krunner
pkgver=5.109.0
pkgrel=0
pkgdesc="Framework for providing different actions given a string query"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kservice-dev
	plasma-framework-dev
	plasma-framework-dev
	qt5-qtbase-dev
	threadweaver-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/krunner.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/krunner-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Requires running dbus instance

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/krunner.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
18e0bb909fae46639d5ad3ef9d2598ab1bb0638ce7bb0ebce6ac3cd028564ddac6814ad4a83a7d6f2234203b6f3f55b6f7057e43cf85a05469cc70bec733a70f  krunner-5.109.0.tar.xz
"
