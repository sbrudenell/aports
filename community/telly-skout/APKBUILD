# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=telly-skout
pkgver=23.04.3
pkgrel=1
pkgdesc="Convergent TV guide based on Kirigami"
url="https://invent.kde.org/plasma-mobile/telly-skout"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="GPL-2.0-or-later AND LicenseRef-KDE-Accepted-GPL"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	ki18n-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/telly-skout.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/telly-skout-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
af96556fb075131aa63ec440d0d67e39f65335ab689a18bdfea58dcfae9200531dd703dcb4af6fedc1a1396fdb8794ec47b9251b27c12f46d4a8817f0e0f68ae  telly-skout-23.04.3.tar.xz
"
