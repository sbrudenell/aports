# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=qqc2-desktop-style
pkgver=5.109.0
pkgrel=0
pkgdesc="A style for Qt Quick Controls 2 to make it follow your desktop theme"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-3.0-only AND (GPL-2.0-only OR GPL-3.0-only)"
depends="
	qt5-qtgraphicaleffects
	qt5-qtquickcontrols2
	"
depends_dev="
	kconfigwidgets-dev
	kiconthemes-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/qqc2-desktop-style.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/qqc2-desktop-style-$pkgver.tar.xz"
subpackages="$pkgname-dev"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/qqc2-desktop-style.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3e50201ef935ed64f392e9cc441110dafb02dd2a97404a70b8380218169913d81c81b2d1c8f68431cb327edc08da5fde5c0a5af033d8d539706e0e39642deeb9  qqc2-desktop-style-5.109.0.tar.xz
"
