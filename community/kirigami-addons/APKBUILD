# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kirigami-addons
pkgver=0.10.0
pkgrel=0
pkgdesc="Add-ons for the Kirigami framework"
url="https://invent.kde.org/libraries/kirigami-addons"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtmultimedia
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami2-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/kirigami-addons/kirigami-addons-$pkgver.tar.xz"

_commit=""
_repo_url="https://invent.kde.org/libraries/kirigami-addons.git"
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# tst_sounds.qml is broken
	ctest --test-dir build --output-on-failure -E "tst_sounds.qml"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
515ddbe99b2c9d07d20742b3530181ba6ec63bbc616cc7e0a590ebfe4c38f7a2ecd8a36e004539df382d7e7bd995674eba2646ce6be72be1a63ef087390cf03e  kirigami-addons-0.10.0.tar.xz
"
