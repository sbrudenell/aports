# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-dnsrobocert
pkgver=3.24.1
pkgrel=0
pkgdesc="A tool to manage your DNS-challenged TLS certificates"
url="https://dnsrobocert.readthedocs.io/en/latest/"
arch="noarch"
license="MIT"
depends="
	certbot
	py3-acme
	py3-boto3
	py3-cffi
	py3-cryptography
	py3-colorama
	py3-coloredlogs
	py3-dnspython
	py3-dns-lexicon
	py3-jsonschema
	py3-localzone
	py3-lxml
	py3-openssl
	py3-pem
	py3-schedule
	py3-softlayer
	py3-softlayer-zeep
	py3-tldextract
	py3-xmltodict
	py3-yaml
	"
makedepends="
	py3-gpep517
	py3-poetry-core
	"
checkdepends="pebble py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/adferrand/dnsrobocert/archive/v$pkgver.tar.gz
	pebble.patch
	revert-old-python.patch
	"
builddir="$srcdir/dnsrobocert-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
a9f7e9772f6a3fed5b07ea2747c99003aa373c4434ee2c30dd126dcbf136eb3192723b737962f67b4192d5d60d535c2c746172ed548f112a2d05b5ea8ce9bcb3  py3-dnsrobocert-3.24.1.tar.gz
3a8f2d9a74a35aea2e5eebcede656d2861382c975dc94560eca4f94cd8b13f1bb4a98b5b667cb5937ef9123a8f1da20dcef58a8ffc903e93e979d928bca9f9b1  pebble.patch
bcaf7e5eccad8a713577269e2458502a089ff59c949c4ea583da6b13d6504f5ad9f905fdf1b0d30c759aa7a1555abcf16cedc11869f6ffef71b09a0204364493  revert-old-python.patch
"
