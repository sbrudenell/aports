# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-rich
pkgver=13.5.2
pkgrel=0
pkgdesc="Python library for rich text formatting and terminal formatting"
url="https://rich.readthedocs.io/en/latest/"
arch="noarch"
license="MIT"
depends="
	py3-markdown-it-py
	py3-pygments
	"
makedepends="py3-gpep517 py3-installer py3-poetry-core py3-wheel"
checkdepends="py3-pytest py3-setuptools"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/willmcgugan/rich/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/rich-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 1>&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -k 'not test_python_render_simple_indent_guides and not test_python_render_line_range_indent_guides'
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
60dfc66836f277763695611f6a3890da21afbb84bd85f38952efdac78680a8e3faca2e8c0aa937d618f876df1f36d3e67986979aa4fa9b6cb7dfb214f348b8aa  py3-rich-13.5.2.tar.gz
"
