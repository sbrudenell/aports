# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdenetwork-filesharing
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/internet/"
pkgdesc="Properties dialog plugin to share a directory with the local network"
license="GPL-2.0-only OR GPL-3.0-only"
depends="samba"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	qcoro-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/network/kdenetwork-filesharing.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenetwork-filesharing-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DSAMBA_INSTALL=OFF
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7b74eb6908a8452a449d905921e4023211bca82e4891cf6900f10376f3875d46fc00b3207807fa433199f10f566fab1cace865e51a4a89b9568e3b92d83bf5be  kdenetwork-filesharing-23.04.3.tar.xz
"
