# Contributor: Anders Björklund <anders.f.bjorklund@gmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=squashfuse
pkgver=0.4.0
pkgrel=0
pkgdesc="FUSE filesystem to mount squashfs archives"
url="https://github.com/vasi/squashfuse"
arch="all"
license="BSD-2-Clause"
makedepends="
	autoconf
	automake
	libtool
	fuse3-dev
	lz4-dev
	lzo-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
checkdepends="squashfs-tools"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/vasi/squashfuse/archive/refs/tags/$pkgver.tar.gz"

prepare() {
	default_prepare
	./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
171850cc6123c0a1bf8d0731293d36d8258e4c953076b59b021564ab5c86d0a36eb7c8a208f1d03b194020071b1a86b82e7c79fe3a09a6090ffbb3a7fdb2a74f  squashfuse-0.4.0.tar.gz
"
